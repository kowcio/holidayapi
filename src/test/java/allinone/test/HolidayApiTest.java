package allinone.test;

import holidayapi.service.HttpsDownloader;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import holidayapi.Application;
import holidayapi.model.HolidayApiResponse;
import holidayapi.model.SameDayHolidayApiRequest;
import holidayapi.service.HolidayAPI;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {Application.class, HolidayAPI.class, HttpsDownloader.class})
@WebAppConfiguration
@SpringBootApplication
@ContextConfiguration(classes = {Application.class, HolidayAPI.class, HttpsDownloader.class})
public class HolidayApiTest {

    private static final String CC2 = "NL";
    private static final String CC1 = "PL";
    public static final String REQ_DATE = "2016-03-28";
    private static Logger log = LoggerFactory.getLogger(HolidayApiTest.class);

    @Autowired
    HolidayAPI holidayAPI;

    @Test
    public void testApiLogicInGeneral() {

        SameDayHolidayApiRequest req = new SameDayHolidayApiRequest(CC1, CC2, REQ_DATE);
        HolidayApiResponse sameHolidayDate = holidayAPI.getSameHolidayDate(req);
        assertEquals(sameHolidayDate.getDate(), "2016-12-25");

    }


    @After
    public void afterTest() {

    }

}
