package holidayapi.constants;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;
import org.joda.time.format.DateTimeParser;

@Singleton
public class HolidayApiConstants {

    public static final String YYYY_MM_DD = "yyyy-MM-dd";
    public static final String DD_MM_YYYY = "dd-MM-yyyy";
    //    Test API Key:
//            41781723-4397-42a4-aa37-92f43e533477
//    Test keys are unmetered and return dummy holiday day.
//    Live API Key:
//            70bb6d9c-aa7d-4c99-8943-1958218ac6e4
//    Limited to 500 calls per month, historical data only.
//
    public static final String KEY_TEST = "41781723-4397-42a4-aa37-92f43e533477";
    public static final String KEY_LIVE = "70bb6d9c-aa7d-4c99-8943-1958218ac6e4";

    public static DateTimeFormatter getDateFormatter() {
        DateTimeParser[] parsers = {
                DateTimeFormat.forPattern(YYYY_MM_DD).getParser(),
                DateTimeFormat.forPattern(DD_MM_YYYY).getParser()};
        return new DateTimeFormatterBuilder().append(null, parsers).toFormatter();
    }


}
