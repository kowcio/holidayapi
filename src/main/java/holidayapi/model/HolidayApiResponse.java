package holidayapi.model;


import org.joda.time.DateTime;

import static holidayapi.constants.HolidayApiConstants.YYYY_MM_DD;

public class HolidayApiResponse {

    private String date;
    private String name1;
    private String name2;

    @Override
    public String toString() {
        return "HolidayApiResponse{" +
                "date='" + date + '\'' +
                ", name1='" + name1 + '\'' +
                ", name2='" + name2 + '\'' +
                '}';
    }

    public HolidayApiResponse() {
    }

    public static HolidayApiResponse build() {
        return new HolidayApiResponse();
    }

    public String getDate() {
        return date;
    }

    public HolidayApiResponse setDate(String date) {
        this.date = date;
        return this;
    }
    public HolidayApiResponse setDate(DateTime date) {
        this.date = date.toString(YYYY_MM_DD);
        return this;
    }
    public String getName1() {
        return name1;
    }

    public HolidayApiResponse setName1(String name1) {
        this.name1 = name1;
        return this;
    }

    public String getName2() {
        return name2;
    }

    public HolidayApiResponse setName2(String name2) {
        this.name2 = name2;
        return this;
    }
}
