package holidayapi.model;

import org.joda.time.DateTime;

import java.util.Arrays;
import java.util.List;

public class SameDayHolidayApiRequest {


    private String cc1;
    private String cc2;
    private String date;
    private DateTime dateTime;//yyyy-MM-dd

    public SameDayHolidayApiRequest() {
    }

    public SameDayHolidayApiRequest(String cc1, String cc2, String date) {
        this.cc1 = cc1;
        this.cc2 = cc2;
        this.date = date;
    }

    public List<String> getCountryCodes() {
        return Arrays.asList(cc1, cc2);
    }

    @Override
    public String toString() {
        return "SameDayHolidayApiRequest{" +
                "cc1='" + cc1 + '\'' +
                ", cc2='" + cc2 + '\'' +
                ", date='" + date + '\'' +
                '}';
    }

    public DateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(DateTime dateTime) {
        this.dateTime = dateTime;
    }

    public String getCc1() {
        return cc1;
    }

    public void setCc1(String cc1) {
        this.cc1 = cc1;
    }

    public String getCc2() {
        return cc2;
    }

    public void setCc2(String cc2) {
        this.cc2 = cc2;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
