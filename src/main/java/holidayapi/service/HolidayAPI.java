package holidayapi.service;


import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import holidayapi.constants.HolidayApiConstants;
import holidayapi.model.HolidayApiResponse;
import holidayapi.model.SameDayHolidayApiRequest;

import java.util.*;

@Service
public class HolidayAPI {
    private static final String COUNTRY_CODE = "COUNTRY_CODE";
    private static final String YEAR = "YEAR";
    private static final String KEY_TEST_OR_PROD = "key_test_or+prod";
    private static final String URL = "https://holidayapi.com/v1/holidays" +
            "?key=" + KEY_TEST_OR_PROD +
            "&country=" + COUNTRY_CODE +
            "&year=" + YEAR +
            "&pretty=1";
    private static final String HOLIDAYS = "holidays";
    private static final String DATE = "date";
    private static final String STATUS = "status";
    private static final String DTF = "yyyy-MM-dd";
    private static final String ERROR = "error";
    private static final String NAME = "name";
    private static final String HTTP_CLIENT_TRUST_CERTS_ERROR_MSG = "Error with trust all certs client. Please notify the admin.";
    private static final String NO_DATES_MATCHES_IN_GIVEN_YEAR = "No dates matches in given year.";
    private final Logger log = LoggerFactory.getLogger(getClass());


    @Autowired
    private HttpsDownloader httpsDownloader;


    public HolidayApiResponse getSameHolidayDate(SameDayHolidayApiRequest req) {

        RestTemplate restTrustAllCertsTemplate;
        try {
            restTrustAllCertsTemplate = httpsDownloader.getTrustAllCertsClient();
        } catch (Exception e) {
            return getHolidayApiResponseExceptionMessage(HTTP_CLIENT_TRUST_CERTS_ERROR_MSG);
        }

        DateTime holidayDateTime = HolidayApiConstants
                .getDateFormatter()
                .parseDateTime(req.getDate());
        req.setDateTime(holidayDateTime);

        List<String> countryCodes = req.getCountryCodes();
        Map<String, Map<DateTime, JSONArray>> countryAndHolidays = new HashMap<>();
        int year = req.getDateTime().getYear();

        for (String countryCode : countryCodes) {

            String uri = URL
                    .replace(YEAR, String.valueOf(year))
                    .replace(KEY_TEST_OR_PROD, HolidayApiConstants.KEY_LIVE)
                    .replace(COUNTRY_CODE, countryCode);

            log.info("Requesting URL : {}", uri);

            ResponseEntity<String> resp;
            try {
                resp = restTrustAllCertsTemplate.getForEntity(uri, String.class);
            } catch (HttpClientErrorException e) {
                return getHolidayApiResponseExceptionMessage(e);
            }

            log.debug("Data for {} : {}", countryCode, resp.getBody());

            JSONObject response = new JSONObject(resp.getBody());

            //holidays sorted by date - "name" of JSON object
            JSONObject holidays = (JSONObject) response.get(HOLIDAYS);
            Iterator<String> holidayDates = holidays.keys();

            Map<DateTime, JSONArray> datesAndHolidays = new TreeMap<>();

            while (holidayDates.hasNext()) {
                String dateWithCountryHolidays = holidayDates.next();
                DateTime date = HolidayApiConstants.getDateFormatter().parseDateTime(dateWithCountryHolidays);
                JSONArray dateHolidays = (JSONArray) holidays.get(dateWithCountryHolidays);
                datesAndHolidays.put(date, dateHolidays);
            }
            countryAndHolidays.put(countryCode, datesAndHolidays);
        }

        Set<DateTime> cc1Dates = countryAndHolidays.get(req.getCc1()).keySet();
        Set<DateTime> cc2Dates = countryAndHolidays.get(req.getCc2()).keySet();
        cc1Dates.retainAll(cc2Dates);//country 1 dates -> only intersections are left in the set
        Set<DateTime> sortedIntersectionDates = new TreeSet<>(cc1Dates);

        DateTime nextDate = null;

        if (sortedIntersectionDates.isEmpty()) {
            return getHolidayApiResponseExceptionMessage(NO_DATES_MATCHES_IN_GIVEN_YEAR);
        }
        HolidayApiResponse localApiResponse = new HolidayApiResponse();

        for (DateTime intersectionDate : sortedIntersectionDates) {
            if (intersectionDate.isAfter(req.getDateTime())) {
                log.info("Found date = {}, originalDate = {}", intersectionDate, req.getDateTime());
                nextDate = intersectionDate;
                String date = intersectionDate.toString(DTF);
                localApiResponse.setDate(date);
                break;
            }
        }

        JSONObject holiday1 = (JSONObject) countryAndHolidays.get(countryCodes.get(0)).get(nextDate).get(0);
        localApiResponse.setName1(holiday1.get(NAME).toString());
        JSONObject holiday2 = (JSONObject) countryAndHolidays.get(countryCodes.get(1)).get(nextDate).get(0);
        localApiResponse.setName2(holiday2.get(NAME).toString());

        log.info("Cross section for all the dates : {}", localApiResponse);

        return localApiResponse;
    }

    private HolidayApiResponse getHolidayApiResponseExceptionMessage(Exception exception) {
        return HolidayApiResponse.build().setDate("Error : " + exception.getMessage());
    }

    private HolidayApiResponse getHolidayApiResponseExceptionMessage(String exceptionMsgForUser) {
        return HolidayApiResponse.build().setDate("Error : " + exceptionMsgForUser);
    }


}
