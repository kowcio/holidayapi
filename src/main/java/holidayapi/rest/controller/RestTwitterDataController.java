package holidayapi.rest.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import holidayapi.model.HolidayApiResponse;
import holidayapi.model.SameDayHolidayApiRequest;
import holidayapi.service.HolidayAPI;


@RestController
@RequestMapping("/rest")
public class RestTwitterDataController {

    @Autowired
    private
    HolidayAPI holidayApi;

    private final Logger log = LoggerFactory.getLogger(getClass());

    /**
     * Example requests in browser : http://127.0.0.1:1234/rest/getHolidayAtTheSameDayInTwoDifferentCountries?cc1=PL&cc2=NL&date=2016-03-28
     * Main website : https://holidayapi.com/
     *
     * @param req - object created from send parameters;
     * @return HolidayApiResponse - date with holidays in both countries.
     */
    @ResponseBody
    @RequestMapping(value = "/getHolidayAtTheSameDayInTwoDifferentCountries", method = RequestMethod.GET)
    public HolidayApiResponse getHolidayAtTheSameDayInTwoDifferentCountries(SameDayHolidayApiRequest req) {
        log.info("Request : {}", req);

        return holidayApi.getSameHolidayDate(req);

    }


}
