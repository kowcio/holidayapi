 ###Testing of HolidayAPI.
 Find holidays in two countries on the same day that are after the specified date.
 
 Import
 Build
 Run test to check.
 Run application and check manually at URL : 
 
 ####request : 
 
 http://127.0.0.1:1234/rest/getHolidayAtTheSameDayInTwoDifferentCountries?cc1=PL&cc2=NL&date=2016-03-28
 
 ###response : 
 
 {"date":"2016-12-25","name1":"Pierwszy dzień Bożego Narodzenia","name2":"Eerste Kerstdag"}
 
 ###Info
 
 Https logic - trusts all certificates on the client side. 
 HttpsDownloader class will always trust a certificate : 
 
    public interface TrustStrategy {
    boolean isTrusted(X509Certificate[] var1, String var2) throws CertificateException;
    }